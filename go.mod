module hugo

go 1.19

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/ekomobile/dadata/v2 v2.9.0
	github.com/go-chi/chi v1.5.5
	github.com/go-chi/jwtauth v1.2.0
	github.com/stretchr/testify v1.8.4
	golang.org/x/crypto v0.18.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/goccy/go-json v0.10.2 // indirect
	github.com/lestrrat-go/backoff/v2 v2.0.8 // indirect
	github.com/lestrrat-go/httpcc v1.0.1 // indirect
	github.com/lestrrat-go/iter v1.0.2 // indirect
	github.com/lestrrat-go/jwx v1.2.28 // indirect
	github.com/lestrrat-go/option v1.0.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
