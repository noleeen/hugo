package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth"
	"hugo/proxy/auth"
	"hugo/proxy/geoservice"
	"hugo/proxy/static"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"
)

func main() {
	r := chi.NewRouter()

	proxy := NewReverseProxy("hugo", "1313")

	// аутентифакация авторизация
	authStorage := &auth.StorageAuth{Users: map[string]auth.User{}} //лучше сделать с помощью конструктора!
	authObject := &auth.Auth{Storage: authStorage}
	auth.Token = jwtauth.New("HS256", []byte("sdf"), nil)

	//os.Setenv("HOST", proxy.host)
	r.Use(proxy.ReverseProxy)

	// СВАГГЕР ПРОПИСАН В ПРОКСИ
	//r.Get("/swagger", static.SwaggerUI)
	//r.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
	//	http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	//})

	r.Route("/api", func(r chi.Router) {
		r.Get("/*", HelloFromApi)

		r.Post("/login", authObject.Login)
		r.Post("/register", authObject.Register)
		r.Route("/address", func(r chi.Router) {
			r.Use(jwtauth.Verifier(auth.Token))
			r.Use(jwtauth.Authenticator)
			// по адресу http://localhost:8080/address/search/ пишем в поисковой строке приблизительный адрес,
			//с помощью geoservice.SearchAddressHandler подирается предполагаемый адрес и показывается на карте.
			//нажимаем на маркер на карте и с помощью geoservice.GeocodeAddressHandler нам показывается список из точных адресов в радиусе 75 метров
			r.Post("/search", geoservice.SearchAddressHandler)
			r.Post("/geocode", geoservice.GeocodeAddressHandler)

		})

	})

	//go workers.WorkerTest()
	server := &http.Server{
		Addr:    ":8080",
		Handler: r,
	}

	//------- gracefulShutdown_1 ----------------------------------------------------------------
	////создаем канал для принятия сигналов о завершении работы
	//signChan := make(chan os.Signal, 1)
	////функция signal.Notify для регистрации обработчика сигналов
	//signal.Notify(signChan, syscall.SIGINT, syscall.SIGTERM)
	//
	////запускаем сервер в горутине
	//go func() {
	//	fmt.Println("server is running on :8080")
	//	err := server.ListenAndServe()
	//	if err != nil {
	//		log.Fatalf("Server error: %v", err)
	//	}
	//}()
	//
	////ожидаем сигнал завершения работы
	//<-signChan
	//fmt.Println("Shutting down server...")
	//
	//// Ожидаем завершения всех активных запросов в течение 5 секунд
	//ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	//defer cancel()
	//
	//if err := server.Shutdown(ctx); err != nil {
	//	fmt.Printf("Error during server shutdown: %v\n", err)
	//}
	//
	//fmt.Println("Server gracefully stopped")
	//-------------------------------------------------------------------------------------------

	//------- gracefulShutdown_2 ----------------------------------------------------------------

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		err := server.ListenAndServe()
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("Server error: %v", err)
		}
	}()
	wg := sync.WaitGroup{}
	go func() {
		wg.Add(1)
		server.RegisterOnShutdown(func() {
			// server.RegisterOnShutdown и server.Shutdown должны успеть завершиться за общее время
			log.Println("То, что нужно сделать до отключения сервера")
			wg.Done()
		})
	}()

	go func() {
		log.Printf("PID:%v", os.Getpid())
	}()
	select {
	case <-signalChan:
		ctx, cancelFunc := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelFunc()
		err := server.Shutdown(ctx)
		if err != nil {
			log.Fatal(err)
		}
	}
	wg.Wait()
	fmt.Println("Server stopped gracefully")

	//-------------------------------------------------------------------------------------------

	//
	//- Как отправить сигнал остановки в докер контейнер
	//
	//docker container stop --signal SIGINT proxy
	//
	//docker stop --s SIGINT proxy
	//
	//docker stop --t 1 proxy
}

type ReverseProxy struct {
	host string
	port string
}

func NewReverseProxy(host, port string) *ReverseProxy {
	return &ReverseProxy{
		host: host,
		port: port,
	}
}

func HelloFromApi(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello from API"))
}

//func (rp *ReverseProxy) ReverseProxy(next http.Handler) http.Handler {
//	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		if !strings.HasPrefix(r.URL.Path, "/api/") && r.URL.Path != "/static/swagger.json" && r.URL.Path != "/swagger" {
//			newURL, err := url.Parse(fmt.Sprintf("http://%s:%s", rp.host, rp.port))
//			if err != nil {
//				log.Printf("Error parsing URL: %v\n", err)
//			}
//			reverseProxy := httputil.NewSingleHostReverseProxy(newURL)
//
//			reverseProxy.ServeHTTP(w, r)
//		} else {
//			next.ServeHTTP(w, r)
//		}
//
//	})
//}

func (rp *ReverseProxy) ReverseProxy(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !strings.HasPrefix(r.URL.Path, "/api/") {
			url, err := url.Parse(fmt.Sprintf("http://%s:%s", rp.host, rp.port))
			if err != nil {
				log.Println(err)
			}
			proxy := httputil.NewSingleHostReverseProxy(url)
			r.Host = "hugo:1313"

			if strings.HasPrefix(r.URL.Path, "/swagger") {
				static.SwaggerUI(w, r)
				return
			}

			if strings.HasPrefix(r.URL.Path, "/static") {
				http.ServeFile(w, r, "/static/swagger.json")
				return
			}

			proxy.ServeHTTP(w, r)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
