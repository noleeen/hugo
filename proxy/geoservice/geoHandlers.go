package geoservice

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/ekomobile/dadata/v2"
	"github.com/ekomobile/dadata/v2/client"
	"io"
	"log"
	"net/http"
)

func SearchAddressHandler(w http.ResponseWriter, r *http.Request) {
	var request SearchRequest

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	//fmt.Println("reqSearch^", request)

	api := dadata.NewCleanApi(client.WithCredentialProvider(&client.Credentials{
		ApiKeyValue:    "a946c983104305a9207502be79c394f0128cc0ff",
		SecretKeyValue: "4cabaeda3a91e75d2a16c1d74aea1203237e3d3b",
	}))

	addresses, err := api.Address(context.Background(), request.Query)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var searchResponse SearchResponse
	searchResponse.Addresses = []*Address{{
		Lat:    addresses[0].GeoLat,
		Lon:    addresses[0].GeoLon,
		Result: addresses[0].Result,
	}}
	//fmt.Println("searchResponse^", searchResponse.Addresses[0].Result)
	err = json.NewEncoder(w).Encode(&searchResponse)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

}

func GeocodeAddressHandler(w http.ResponseWriter, r *http.Request) {
	var request GeocodeRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	//fmt.Printf("requestGeo: %#v\n", request)
	//ОШИБКА в скрипте search.md  там вместо lon указано lng, а с lng не считывается dadata
	//по-хорошему нужно исправить код фронта на search.md, но я там не особо разобрался,
	//поэтому сделал структуру-адаптер которая с реквеста считывает lng записывает в lon и отправляет на dadata
	adapter := &Adapter{
		Lat:           request.Lat,
		Lon:           request.Lng,
		Radius_meters: "75",
	}

	marshal, err := json.Marshal(adapter)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	url := "http://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address"
	requestByDadata, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(marshal))
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	requestByDadata.Header.Add("Content-Type", "application/json")
	requestByDadata.Header.Add("Accept", "application/json")
	requestByDadata.Header.Add("Authorization", "Token a946c983104305a9207502be79c394f0128cc0ff")

	newClient := &http.Client{}

	response, err := newClient.Do(requestByDadata)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	respBody, err := io.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var suggestions Suggestions
	err = json.Unmarshal(respBody, &suggestions)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	//fmt.Println("suggestion^", suggestions)
	adresses := &GeocodeResponse{}
	for _, adr := range suggestions.Suggestions {
		adresses.Addresses = append(adresses.Addresses,
			&Address{
				Result: adr.Value,
				Lat:    *adr.Data["geo_lat"],
				Lon:    *adr.Data["geo_lon"],
			})
	}

	err = json.NewEncoder(w).Encode(&adresses)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}
