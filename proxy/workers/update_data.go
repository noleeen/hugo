package workers

import (
	"fmt"
	"log"
	"os"
	"time"
)

const content = `---
menu:
    before:
        name: tasks
        weight: 5
title: Обновление данных в реальном времени
---

# Задача: Обновление данных в реальном времени

Напишите воркер, который будет обновлять данные в реальном времени, на текущей странице.
Текст данной задачи менять нельзя, только время и счетчик.

Файл данной страницы: \"/app/static/tasks/_index.md\"

Должен меняться счетчик и время:

Текущее время: %s

Счетчик: %d



## Критерии приемки:
- [ ] Воркер должен обновлять данные каждые 5 секунд
- [ ] Счетчик должен увеличиваться на 1 каждые 5 секунд
- [ ] Время должно обновляться каждые 5 секунд`

var content2 = `---
menu:
    after:
        name: graph
        weight: 1
title: Построение графа
---

# Построение графа: 
{{< columns >}}
{{< mermaid >}}
graph LR
%s
{{< /mermaid >}}
{{< /columns >}}`

var content3 = `---
menu:
    after:
        name: binary_tree
        weight: 2
title: Построение сбалансированного бинарного дерева
---
{{< mermaid >}}
graph TD
%s

{{< /mermaid >}}
`

func WorkerTest() {
	t := time.NewTicker(5 * time.Second)
	var b byte = 0
	n := 5
	for {
		select {
		case <-t.C:
			err := os.WriteFile(
				"/app/static/tasks/_index.md",
				[]byte(fmt.Sprintf(content, time.Now().Format("02-01-2006 15:04:05"), b)),
				0644)
			if err != nil {
				log.Println(err)
			}
			b++

			graph := NewGraph()
			err = os.WriteFile(
				"/app/static/tasks/graph.md",
				[]byte(fmt.Sprintf(content2, graph.CreateRandGraph(5, 30))),
				0644)
			if err != nil {
				log.Println(err)
			}

			tree := GenerateTree(n)
			err = os.WriteFile(
				"/app/static/tasks/binary.md",
				[]byte(fmt.Sprintf(content3, tree.ToMermaid())),
				0644)
			if err != nil {
				log.Println(err)
			}
			n++
			if n > 100 {
				n = 5
			}
		}
	}
}
