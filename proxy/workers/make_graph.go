package workers

import (
	"fmt"
	"math/rand"
	"time"
)

type Graph struct {
	Nodes []*Node
	Edges []*Edge
}

type Node struct {
	ID   int
	Name string
	Form []string // "circle", "rect", "square", "ellipse", "round-rect", "rhombus"
	//Links []*Node
}

type Edge struct {
	From, To *Node
}

func NewGraph() *Graph {
	return &Graph{}
}

func (g *Graph) AddNode(node *Node) {
	g.Nodes = append(g.Nodes, node)
}
func (g *Graph) AddEdge(from, to *Node) {
	edge := &Edge{From: from, To: to}
	g.Edges = append(g.Edges, edge)
}

func (g *Graph) CreateRandGraph(from, to int) string {
	rand.Seed(time.Now().UnixNano())

	//figure := []string{"(Round Rect)", "{Rhombus}", "((Circle))", "[Square Rect]", "{{Форма 9}}", ">Форма 7]", "([Форма 3])"}
	figureMap := map[int][]string{
		0: {"(", ")"},
		1: {"{", "}"},
		2: {"((", "))"},
		3: {"[", "]"},
		4: {"{{", "}}"},
		5: {">", "]"},
		6: {"([", "])"},
	}

	countNodes := rand.Intn(to-from) + from
	for i := 0; i < countNodes; i++ {
		node1 := &Node{
			ID:   i,
			Name: fmt.Sprintf("name%d", i),
			Form: figureMap[rand.Intn(len(figureMap))],
		}
		g.AddNode(node1)
	}

	for i := 0; i < countNodes; i++ {
		for j := i + 1; j < countNodes; j++ {
			if rand.Intn(6) == 0 {
				g.AddEdge(g.Nodes[i], g.Nodes[j])

			} else if rand.Intn(6) == 1 {
				g.AddEdge(g.Nodes[j], g.Nodes[i])

			}
		}
	}

	return g.PrintGraph()

}

func (g *Graph) PrintGraph() string {
	res := ""
	for _, edge := range g.Edges {
		res += fmt.Sprintf("%d%s%s%s-->%d%s%s%s\n",
			edge.From.ID, edge.From.Form[0], edge.From.Name, edge.From.Form[1],
			edge.To.ID, edge.To.Form[0], edge.To.Name, edge.To.Form[1])
	}
	return res
}
