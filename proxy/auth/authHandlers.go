package auth

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"time"
)

var Token *jwtauth.JWTAuth

func (a *Auth) Register(w http.ResponseWriter, r *http.Request) {
	//РЕГИСТРАЦИЯ НОВОГО ПОЛЬЗОВАТЕЛЯ

	var registerRequest User

	err := json.NewDecoder(r.Body).Decode(&registerRequest)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if registerRequest.Name == "" || registerRequest.Password == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("name or pasword is empty"))
		return
	}

	if _, ok := a.Storage.Users[registerRequest.Name]; ok {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(fmt.Sprintf("user with name %s already exist", registerRequest.Name)))
		return
	}

	//а если такого пользователя в базе нет и имя и пароль не пусты, то создаём в хранилище такого пользователя
	hashPass, err := bcrypt.GenerateFromPassword([]byte(registerRequest.Password), bcrypt.DefaultCost)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	a.Storage.Users[registerRequest.Name] = User{
		Name:     registerRequest.Name,
		Password: string(hashPass),
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("user successfully created"))

}

func (a *Auth) Login(w http.ResponseWriter, r *http.Request) {
	logRequest := User{}
	err := json.NewDecoder(r.Body).Decode(&logRequest)
	if err != nil {
		http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		return
	}

	if logRequest.Name == "" || logRequest.Password == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("name or pasword is empty"))
		return
	}

	user, ok := a.Storage.Users[logRequest.Name]
	if !ok {
		http.Error(w, "user not found", http.StatusUnauthorized)
		return
	}

	//проверяем одинаковые ли пароли, если нет - отправляем соответсвующую ошибку
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(logRequest.Password))
	if err != nil {
		http.Error(w, "incorrect password", http.StatusUnauthorized)
		return
	}

	_, tokenStr, err := Token.Encode(jwt.MapClaims{
		"name": user.Name,
		"exp":  time.Now().Add(time.Hour * 24).Unix(),
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Println()

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, tokenStr)
}
