package auth

type Auth struct {
	Storage *StorageAuth
}

type StorageAuth struct {
	Users map[string]User
}

type User struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}
