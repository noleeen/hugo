package auth

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAuth_Register(t *testing.T) {
	storage := map[string]User{
		"testUser": {
			Name:     "testUser",
			Password: "testPass",
		},
	}
	auth := &Auth{Storage: &StorageAuth{Users: storage}}

	tests := []struct {
		name         string
		requestBody  interface{} //тело запроса json
		expectedCode int
		expectedBody string
	}{
		{
			name:         "пустой запрос",
			requestBody:  nil,
			expectedCode: http.StatusBadRequest,
			expectedBody: "name or pasword is empty",
		},
		{
			name: "пользователь уже существует",
			requestBody: User{
				Name:     "testUser",
				Password: "1",
			},
			expectedCode: http.StatusOK,
			expectedBody: "user with name testUser already exist",
		},
		{
			name: "добаввление пользователя",
			requestBody: User{
				Name:     "testUser2",
				Password: "12",
			},
			expectedCode: http.StatusOK,
			expectedBody: "user successfully created",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Преобразование тела запроса в JSON
			marshal, err := json.Marshal(tt.requestBody)
			assert.NoError(t, err)

			// Создание HTTP-запроса с тестовыми данными
			request, err := http.NewRequest("POST", "/register", bytes.NewBuffer(marshal))
			assert.NoError(t, err)

			// Создание HTTP-ответа
			recorder := httptest.NewRecorder()

			// Вызов функции Register
			auth.Register(recorder, request)

			// Проверка статус-кода
			assert.Equal(t, tt.expectedCode, recorder.Code)

			// Проверка тела ответа
			assert.Equal(t, tt.expectedBody, recorder.Body.String())

		})
	}

}
func TestAuth_Login(t *testing.T) {
	storage := map[string]User{
		"testUser": {
			Name:     "testUser",
			Password: "testPass",
		},
	}
	auth := &Auth{Storage: &StorageAuth{Users: storage}}

	tests := []struct {
		name         string
		requestBody  interface{} //тело запроса json
		expectedCode int
		expectedBody string
	}{
		{
			name:         "пустой запрос",
			requestBody:  nil,
			expectedCode: http.StatusBadRequest,
			expectedBody: "name or pasword is empty",
		},
		{
			name: "пользователь не найден",
			requestBody: User{
				Name:     "testUser2",
				Password: "1",
			},
			expectedCode: http.StatusUnauthorized,
			expectedBody: "user not found\n",
		},
		{
			name: "неверный пароль",
			requestBody: User{
				Name:     "testUser",
				Password: "12",
			},
			expectedCode: http.StatusUnauthorized,
			expectedBody: "incorrect password\n",
		},
		{
			//ДОДЕЛАТЬ чтоб проверялся хэшированный пароль и наличие токена
			name: "пользователь вошёл и получил токен",
			requestBody: User{
				Name:     "testUser",
				Password: "testPass",
			},
			expectedCode: http.StatusUnauthorized,
			expectedBody: "incorrect password\n",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Преобразование тела запроса в JSON
			marshal, err := json.Marshal(tt.requestBody)
			assert.NoError(t, err)

			// Создание HTTP-запроса с тестовыми данными
			request, err := http.NewRequest("POST", "/register", bytes.NewBuffer(marshal))
			assert.NoError(t, err)

			// Создание HTTP-ответа
			recorder := httptest.NewRecorder()

			// Вызов функции Register
			auth.Login(recorder, request)

			// Проверка статус-кода
			assert.Equal(t, tt.expectedCode, recorder.Code)

			// Проверка тела ответа
			assert.Equal(t, tt.expectedBody, recorder.Body.String())

		})
	}

}
